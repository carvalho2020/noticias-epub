#!/usr/bin/python3

from bs4 import BeautifulSoup
import urllib.request
from ebooklib import epub
from datetime import datetime
import html
#import sys

url = "https://www.noticiasaominuto.com/rss/ultima-hora"
output = "noticias.epub"
#url = sys.argv[1]
#output = sys.argv[2]

input = urllib.request.urlopen(url)
xml_doc = input.read()

soup = BeautifulSoup(xml_doc, 'html.parser')

data = datetime.now().strftime("%d %h %H:%M")
titulo = soup.title.string + " " + data

book = epub.EpubBook()
book.set_title(titulo)
book.set_language('pt')
book.add_author("Feed")


toc = []
spine = []
num = 0

c1 = epub.EpubHtml(title='Capa', file_name='intro.xhtml', lang='hr')
c1.content= '<html><head></head><body><h1>' + soup.title.string + '</h1><br><br><h2>' + data + '</body></html>'

book.add_item(c1)
spine.append(c1)
spine.append('nav')
toc.append(epub.Section('Capa'))
toc.append(c1)


for noticia in soup.find_all('item') :
        num = num + 1
        t1 = html.unescape(noticia.title.string)
        #t1 = noticia.title.string
        c1 = epub.EpubHtml(title=t1, file_name=f"{num}.xhtml", lang='pt')

        c1.content = "<h1>" + noticia.title.string + "</h1>" + noticia.find('content:encoded').string

        book.add_item(c1)

        spine.append(c1)
        toc.append(epub.Section(t1))
        toc.append(c1)

book.toc = tuple(toc)
book.spine = spine
book.add_item(epub.EpubNcx())
book.add_item(epub.EpubNav())

epub.write_epub(output, book, {})
